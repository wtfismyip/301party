This is a web server that provides functionality of an intentionally open redirect. This may be useful in testing SSRF vulnerabilities.

A live example of this repository lives on https://301party.com

Contact: clint@wtfismyip.com
